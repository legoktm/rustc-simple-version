// License: Apache-2.0

//! # rustc-simple-version
//! Access the version of rustc used to build your application as a
//! straightforward constant, no extra build script needed.
//!
//! ```
//! use rustc_simple_version::RUSTC_VERSION;
//!
//! println!("Built using {}", RUSTC_VERSION);
//! ```
//!
//! The value will be the exact same output as `rustc --version`.

/// rustc version
/// Should be identical to the output of `rustc --version`.
/// # Example
/// ```
/// # use rustc_simple_version::RUSTC_VERSION;
/// println!("Built using {}", RUSTC_VERSION);
/// ```
pub const RUSTC_VERSION: &str = env!("RUSTC_SIMPLE_VERSION");

#[cfg(test)]
mod tests {
    use super::RUSTC_VERSION;

    #[test]
    fn test_rustc_version() {
        assert!(RUSTC_VERSION.starts_with("rustc "));
    }
}
