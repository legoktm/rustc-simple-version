// License: Apache-2.0
use std::process::Command;
use std::str::from_utf8;

fn main() {
    let rustc = option_env!("RUSTC").unwrap_or("rustc");
    let result = Command::new(rustc)
        .arg("--version")
        .output()
        .expect("Failed to run `rustc --version`");
    let version = from_utf8(&result.stdout).unwrap().trim();
    println!("cargo:rustc-env=RUSTC_SIMPLE_VERSION={}", version);
}
