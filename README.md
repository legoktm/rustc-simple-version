rustc-simple-version
============
[![crates.io](https://img.shields.io/crates/v/rustc-simple-version.svg)](https://crates.io/crates/rustc-simple-version)
[![docs.rs](https://docs.rs/rustc-simple-version/badge.svg)](https://docs.rs/rustc-simple-version)
[![pipeline status](https://gitlab.com/legoktm/rustc-simple-version/badges/master/pipeline.svg)](https://gitlab.com/legoktm/rustc-simple-version/-/commits/master)
[![coverage report](https://gitlab.com/legoktm/rustc-simple-version/badges/master/coverage.svg)](https://legoktm.gitlab.io/rustc-simple-version/coverage/)

The `rustc-simple-version` crate provides a simple constant that exposes the
version of `rustc` used to compile your project.

```
use rustc_simple_version::RUSTC_VERSION;

println!("Built using {}", RUSTC_VERSION);
```

The main difference between this and [`rustc_version`](https://crates.io/crates/rustc_version)
is that the latter is intended to be used as a build dependency for your
`build.rs`, while this crate should be used at runtime.

## License
rustc-simple-version is (C) 2020 Kunal Mehta, released under the Apache 2.0 or any later version, see LICENSE for details.
